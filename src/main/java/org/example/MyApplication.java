package org.example;

import org.apache.camel.main.Main;

public final class MyApplication {

    private MyApplication() {}

    public static void main(String[] args) throws Exception {
        Main main = new Main(MyApplication.class);
        main.run(args);
    }
}