package org.example;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.rest.RestBindingMode;

public class MyRouteBuilder extends RouteBuilder {

    @Override
    public void configure() throws Exception {

        restConfiguration().component("jetty")
                .bindingMode(RestBindingMode.auto)
                .dataFormatProperty("prettyPrint", "true")
                .host("localhost")
                .port(8080)

        ;

/*        from("timer:foo?period={{myPeriod}}").id("TimerRoute")
            .bean("myBean", "hello")
            .log("${body}")
            .bean("myBean", "bye")
            .log("${body}")
        ;*/

        from("servlet:hello").id("hello")
                .process(new Processor() {
            public void process(Exchange exchange) throws Exception {
                // Access HTTP headers sent by the client
                Message message = exchange.getMessage();
                String contentType = message.getHeader(Exchange.CONTENT_TYPE, String.class);
                String httpUri = message.getHeader(Exchange.HTTP_URI, String.class);

                // Set the response body
                message.setBody("Hello World");
            }
        });

     //   from("rest://method:path[:uriTemplate]?[options]")
     //   from("rest://get:/outbound/transactions?consumes=application/json")
        rest("/transactions")
                .get().consumes("application/json").to("direct:getTransactions")
                .post().consumes("application/json").to("direct:updateTransactions")
        ;

        from("direct:getTransactions").id("getTransactions")
                .log("getTransactions")
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(200))
                .transform(constant("Completed Request successfully"))
        ;

        from("direct:updateTransactions").id("updateTransactions")
                .log("updateTransactions")
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(200))
                .transform(constant("Completed Request successfully"))
        ;
    }
}
