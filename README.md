
## Introduction to Apache Camel 

Apache Camel is integration frameworks based on EIP - [Enterprice Integration Patterns](https://camel.apache.org/components/4.4.x/eips/enterprise-integration-patterns.html)
It is no way limited or specially designed for HTTP or REST, those are just one of the many features it has. It is an integration framework.
The core functionality is transformers, being able to transform messages from/to almost anything. 
You do not program Camel, you configure it. You write configuration, not code.  
There are several ways of writing the Camel configuration, there are several [DSLs](https://camel.apache.org/components/4.4.x/others/dsl.html) for writing Camel configuration.   
In this example we are shown two: Java DSL and REST DSL. The Camel Java DSL JAva code written using Fluent interfaces.   
[REST DSL](https://camel.apache.org/manual/rest-dsl.html) is more convenient way of writing REST definitions with Camel, there definition would just as well be written using Java DSL.  

 


## Getting started

1. define the Apache Camel BOM for maven 
2. Camel-engine and camel-core, log and test libs
3. Define Camel components you want to use: REST 
4. 



